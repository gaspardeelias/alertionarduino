//
//  actuator.cpp
//  Alertion
//
//  Created by Gaspar de Elias on 06/05/2015.
//  Copyright (c) 2015 Cresta. All rights reserved.
//

#include "siren.h"

Siren::Siren(void)
{
    
}

Siren::Siren(int pin, String name)
{
    mPin = pin;
    mName = name;
}

String Siren::getName()
{
    return mName;
}

void Siren::setTriggered(bool enable)
{
    mIsTriggered = enable;
}

bool Siren::isTriggered()
{
    return mIsTriggered;
}