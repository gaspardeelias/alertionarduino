//
//  sensor.cpp
//  Alertion
//
//  Created by Gaspar de Elias on 06/05/2015.
//  Copyright (c) 2015 Cresta. All rights reserved.
//

#include "sensor.h"

Sensor::Sensor()
{
    
}

Sensor::Sensor(int pin, String name)
{
    mPin = pin;
    mName = name;
    pinMode(pin, INPUT_PULLUP);
}

bool Sensor::isTriggered()
{
    bool result = false;
    if (digitalRead(mPin) == HIGH)
    {
        result = true;
    }
    return result;
}

String Sensor::getName()
{
    return mName;
}