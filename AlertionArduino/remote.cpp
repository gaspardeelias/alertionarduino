//
//  remote.cpp
//  Alertion
//
//  Created by Gaspar de Elias on 06/05/2015.
//  Copyright (c) 2015 Cresta. All rights reserved.
//

#include "remote.h"
#include <Arduino.h>

Remote::Remote()
{
}

Remote::Remote(int pin, String name)
{
    mPin = pin;
    mName = name;
    pinMode(mPin, INPUT_PULLUP);
}

Command Remote::getCommand()
{
    Command result = mPendingCommand;
    mPendingCommand = NONE;
    return result;
}

void Remote::pushCommand(Command command)
{
    mPendingCommand = command;
}

void Remote::run()
{
    uint8_t value = digitalRead(mPin);
    String temp1 = "    Remote::run() readPin: ";
    String debug = temp1 + value;
    DEBUG(debug);
    if (value== LOW && canRead()) {
        mPendingCommand = ARM;
        mLastCommand = millis();
    } else
    {
        mPendingCommand = NONE;
    }
}

/*
 Will read commands only if COMMAND_IGNORE_SECS have passed since the last command.
 This code is to ignore long presses.
 */
bool Remote::canRead()
{
    return ((millis() - mLastCommand) > COMMAND_IGNORE_SECS * 1000);
}
