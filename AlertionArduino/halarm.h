//
//  alarm.h
//  Alertion
//
//  Created by Gaspar de Elias on 06/05/2015.
//  Copyright (c) 2015 Cresta. All rights reserved.
//

#ifndef __Alertion__alarm__
#define __Alertion__alarm__

#include "sensor.h"
#include "siren.h"
#include "remote.h"


#define MAX_ACTUATORS 3
#define MAX_SENSORS 10
#define DEBUG(x) Serial1.println(x);
#define ARM_TIME_DEFAULT 5


enum State {INIT, DISARMED, ARMING, ARMED, TRIGGERED};

class HAlarm
{
public:
    HAlarm(void);
    void addSensor(uint8_t pin, String name);
    void addSiren(uint8_t pin, String name);
    void setRemote(uint8_t pin, String name);
    void run(void);
    void arm(void);
    void disarm(void);
    bool isArmed();
private:
    void checkRTC(void);
    void updateLeds(void);
    State mState = INIT;
    int mNumSirens = 0;
    int mNumSensors = 0;
    Siren mSirens[MAX_ACTUATORS];
    Sensor mSensors[MAX_SENSORS];
    Remote mRemote;
    uint8_t mArmCounter=0;
    unsigned long mLastArmCommand = 0;
};


#endif /* defined(__Alertion__alarm__) */
