//
//  alarm.cpp
//  Alertion
//
//  Created by Gaspar de Elias on 06/05/2015.
//  Copyright (c) 2015 Cresta. All rights reserved.
//

#include "halarm.h"
#include <Arduino.h>
#include <Wire.h>
#include <Time.h>
#include <DS1307RTC.h>


HAlarm::HAlarm(void)
{
    pinMode(51, OUTPUT);
    pinMode(52, OUTPUT);
    pinMode(53, OUTPUT);
}

void HAlarm::addSensor(uint8_t pin, String name)
{
    if(mNumSensors >= MAX_SENSORS)
    {
        // print error
    } else {
        mSensors[mNumSensors++] = Sensor(pin, name);
    }
}

void HAlarm::addSiren(uint8_t pin, String name)
{
    if (mNumSirens >= MAX_ACTUATORS) {
        // print error
    } else {
        mSirens[mNumSirens++] = Siren(pin, name);
    }
}

void HAlarm::setRemote(uint8_t pin, String name)
{
    mRemote = Remote(pin, name);
}

void HAlarm::run(void)
{
    String temp1 = "  state: ";
    String debug = temp1 + mState;
    DEBUG(" HAlarm run() START");
    DEBUG(debug);
    bool triggered = false;
    mRemote.run();
    

    //checkRTC();
    
    switch (mState) {
        case INIT:
            mState = DISARMED;
            if (mRemote.getCommand() == ARM)
            {
                mLastArmCommand = millis();
                mState = ARMING;
            }
            break;
        case ARMING:
            
            if(millis() - mLastArmCommand > (ARM_TIME_DEFAULT * 1000))
            {
                mLastArmCommand = millis();
                mState = ARMED;
            }
            break;
        case ARMED:
            triggered = false;
            for (int i=0; i<mNumSensors; i++) {
                if (mSensors[i].isTriggered())
                {
                    triggered = true;
                    break;
                }
            }
            if (triggered) {
                mState = TRIGGERED;
            }
            if (mRemote.getCommand() == ARM)
            {
                mState = DISARMED;
            }
            break;
        case DISARMED:
            if (mRemote.getCommand() == ARM)
            {
                mLastArmCommand = millis();
                mState = ARMING;
            }
            break;
        case TRIGGERED:
            if (mRemote.getCommand() == ARM)
            {
                mState = DISARMED;
            }
            break;
        default:
            break;
    }
    updateLeds();
    DEBUG(" HAlarm run() END");
}

void HAlarm::arm()
{
    mState = ARMING;
}

void HAlarm::disarm()
{
    mState = DISARMED;
}

bool HAlarm::isArmed()
{
    return (mState == ARMED);
}

void HAlarm::checkRTC()
{
    tmElements_t tm;
    
    if (RTC.read(tm))
    {
        
        DEBUG("RTC OK");
        // RTC ok.
    } else {
        if(RTC.chipPresent())
        {
            // not initialized
        } else {
            // not present
        }
    }
}

void HAlarm::updateLeds()
{
    digitalWrite(51, LOW);
    digitalWrite(52, LOW);
    digitalWrite(53, LOW);
    switch(mState)
    {
        case ARMING:
            //digitalWrite(51, LOW);
            digitalWrite(52, HIGH);
            //digitalWrite(53, LOW);
            break;
        case ARMED:
            digitalWrite(51, HIGH);
            //digitalWrite(52, LOW);
            //digitalWrite(53, LOW);
            break;
        case TRIGGERED:
            //digitalWrite(51, LOW);
            //digitalWrite(52, LOW);
            digitalWrite(53, HIGH);
            break;
        case DISARMED:
        default:
            //digitalWrite(51, LOW);
            //digitalWrite(52, LOW);
            //digitalWrite(53, LOW);
            break;
    }
}
