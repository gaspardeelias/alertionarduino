//
//  actuator.h
//  Alertion
//
//  Created by Gaspar de Elias on 06/05/2015.
//  Copyright (c) 2015 Cresta. All rights reserved.
//

#ifndef __Alertion__actuator__
#define __Alertion__actuator__

#include <Arduino.h>

class Siren
{
public:
    Siren(void);
    Siren(int,String);
    String getName();
    bool isTriggered();
    void setTriggered(bool);
    
private:
    int mPin;
    String mName;
    bool mIsTriggered;
};

#endif /* defined(__Alertion__actuator__) */
