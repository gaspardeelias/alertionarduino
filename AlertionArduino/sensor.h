//
//  sensor.h
//  Alertion
//
//  Created by Gaspar de Elias on 06/05/2015.
//  Copyright (c) 2015 Cresta. All rights reserved.
//

#ifndef __Alertion__sensor__
#define __Alertion__sensor__

#include <Arduino.h>

class Sensor
{
public:
    Sensor(void);
    Sensor(int,String);
    bool isTriggered();
    String getName();
    
private:
    int mPin;
    String mName;
    
};

#endif /* defined(__Alertion__sensor__) */
