//
//  remote.h
//  Alertion
//
//  Created by Gaspar de Elias on 06/05/2015.
//  Copyright (c) 2015 Cresta. All rights reserved.
//

#ifndef __Alertion__remote__
#define __Alertion__remote__

#include <Arduino.h>

#define DEBUG(x) Serial1.println(x)
#define COMMAND_IGNORE_SECS 3

enum Command{NONE,ARM, DISARM};

class Remote
{
public:
    Remote();
    Remote(int pin, String name);
    Command getCommand(void);
    void pushCommand(Command command);
    void run();
    
private:
    String mName;
    int mPin;
    Command mPendingCommand = NONE;
    unsigned long mLastCommand=0;
    bool canRead();
};


#endif /* defined(__Alertion__remote__) */
